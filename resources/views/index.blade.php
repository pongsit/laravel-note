@extends('layouts.main')

@section('content')

<div class="container d-none d-md-block">
    <div class="row justify-content-center">
        <div class="col-xl-7">
            <div class="card shadow">
                <div class="card-header pt-3 pb-3 justify-content-between">
                    <div class="row mb-0">
                        <div class="col">
                            <div class="d-flex justify-content-center">
                                <div class="d-inline-block">
                                    <div class="mr-2" style="width:240px;">
                                        กำหนดความสามารถ
                                    </div>
                                </div>
                                @foreach($roles as $v)
                                    <div class="d-inline-block">
                                        <div class="mr-2 text-center" style="width:100px;">
                                            {{$v->name_show}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form class="d-none d-md-block" action="{{route('role.ability.update')}}" method="post">
                        @php 
                            $bg = '';
                            $bg_mem = $bg; 
                        @endphp
                        @foreach($abilities as $v)
                            <div class="row mb-0" style="{{$bg}}">   
                                <div class="col">
                                    <div class="d-flex justify-content-center">
                                        <div class="d-inline-block">
                                            <div class="mr-2" style="width:240px;">
                                                {{$v->name}}
                                            </div>
                                        </div>
                                        @foreach($ability_checks[$v->id] as $_k => $_v)
                                            <div class="d-inline-block">
                                                <div class="mr-2 text-center" style="width:100px;">
                                                    <input type="checkbox" class="form-check-input" name="{{$v->id}}_{{$_k}}"
                                                        @if($_v) checked @endif
                                                    >
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @php 
                                if(!empty($bg)){
                                    $bg = $bg_mem;
                                }else{
                                    $bg = 'background-color:rgb(33,37,41,0.6);';
                                }
                            @endphp  
                        @endforeach
                    </div>
                    <div class="container">  
                        <div class="row">
                            <div class="col">
                                <div class="d-flex justify-content-center">
                                    <div class="d-inline-block pt-4">
                                        <input type="submit" class="px-4 bg-dark text-white rounded text-center" value="ส่งข้อมูล">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="container sticky-top">
    <div class="row mb-0 bg-dark border-bottom border-top border-secondary">
        <div class="col">
            <div class="d-flex justify-content-center">
                <div class="d-inline-block">
                    <div class="mr-2 py-1">
                        กำหนดความสามารถให้บทบาทต่างๆ
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
{{-- <div class="container d-none d-md-block">
    <div class="row mb-0 bg-dark border-bottom border-secondary">
        <div class="col">
            <div class="d-flex justify-content-center">
                <div class="d-inline-block">
                    <div class="mr-2" style="width:240px;">
                        ความสามารถ
                    </div>
                </div>
                @foreach($roles as $v)
                    <div class="d-inline-block">
                        <div class="mr-2 text-center" style="width:100px;">
                            {{$v->name_show}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div> --}}
<div class="container d-md-none">
     <div class="row justify-content-center">
        <div class="col">
            <div class="card shadow">
                <div class="card-header pt-3 pb-3 text-center justify-content-between">
                    กำหนดความสามารถ
                </div>
                <div class="card-body px-0">
                    <form action="{{route('role.ability.update')}}" method="post">
                        @php 
                            $bg = '';
                            $bg_mem = $bg; 
                        @endphp
                        @foreach($abilities as $v)
                        <div class="pb-1" style="{{$bg}}"> 
                            <div class="container">
                                <div class="mb-2" >
                                    <div class="row mb-0" >   
                                        <div class="col-12">
                                            {{$v->name}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        @foreach($ability_checks[$v->id] as $_k => $_v)
                                            <div class="col-6 col-sm-3">
                                                <input type="checkbox" class="form-check-input" name="{{$v->id}}_{{$_k}}"
                                                    @if($_v) checked @endif
                                                > {{$roles->where('id',$_k)->first()->name_show}}
                                            </div>
                                        @endforeach
                                    </div> 

                                    @php 
                                        if(!empty($bg)){
                                            $bg = $bg_mem;
                                        }else{
                                            $bg = 'background-color:rgb(33,37,41,0.6);';
                                        }
                                    @endphp   
                                </div>
                            </div>
                        </div>
                        @endforeach      
                        <div class="row">
                            <div class="col">
                                <div class="d-flex justify-content-center">
                                    <div class="d-inline-block pt-4">
                                        <input type="submit" class="px-4 bg-dark text-white rounded text-center" value="ส่งข้อมูล">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop