<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" id="navbarDropdownBlog" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Admin
    </a>
    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownBlog">
        <li><a class="dropdown-item" href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li><a class="dropdown-item" href="{{route('admin.member')}}">รายชื่อผู้ใช้</a></li>
    </ul>
</li>