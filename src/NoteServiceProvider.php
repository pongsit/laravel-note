<?php

namespace Pongsit\Note;

use GuzzleHttp\Middleware;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\Note\Providers\EventServiceProvider;

class NoteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->mergeConfigFrom(
        //     __DIR__.'/../config/role.php', 'services'
        // );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->namespace('Pongsit\\Note\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'note');
        $this->publishes([
            __DIR__.'/../config/note.php' => config_path('note.php'),
        ], 'public');
    }
}