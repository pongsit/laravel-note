<?php

namespace Pongsit\Note\Models;

use Pongsit\Note\Database\Factories\NoteFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pongsit\User\Models\User;


class Note extends Model
{
  protected $guarded = [];

  public function notable(){
    return $this->morphTo();
  }

  public function user(){
    return $this->belongsTo('Pongsit\User\Models\User');
  }

}
